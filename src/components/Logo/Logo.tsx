import React from 'react';

const logoImage = require('../../assets/Show dos Memes.svg').default;

const Logo = (props: any) => (
  <img style={{ width: '419px' }} {...props} src={logoImage} alt="Logo" />
);

export default Logo;

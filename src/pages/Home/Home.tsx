import React, { useCallback, useState } from 'react';
import { useHistory } from 'react-router-dom';
import {
  Button,
  Col,
  Container,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  Row,
} from 'reactstrap';
import { Logo } from '../../components';
import styles from './Home.module.scss';

const memeButton = require('../../assets/meme-button.svg').default;

const Home = () => {
  const history = useHistory();
  const [isModalOpen, setModalOpen] = useState(false);
  const onStart = useCallback(() => {
    history.push('/questionary');
  }, [history]);

  return (
    <div>
      <div className={styles.Background} />
      <Modal isOpen={isModalOpen} toggle={() => setModalOpen(false)}>
        <ModalHeader toggle={() => setModalOpen(false)}>Instruções</ModalHeader>
        <ModalBody>
          <p>Cada questão tem apenas 1 alternativa correta.</p>
          <p> Você tem 45 segundos por questão.</p>
          <p> Quanto mais rápido responder, maior será sua pontuação.</p>
          <p>
            Você tem 3 formas de obter ajuda:
            <ol>
              <li>Retirar duas opções erradas (50/50)</li>
              <li>Pular para a próxima questão</li>
              <li>Adicionar 10 segundos no tempo</li>
            </ol>
          </p>
        </ModalBody>
        <ModalFooter>
          <Button color="warning" onClick={onStart}>
            BORA PRO JOGO
            <img src={memeButton} width={32} alt="button meme" className="ml-2" />
          </Button>
        </ModalFooter>
      </Modal>
      <Container className="h-100 mt-auto mb-auto">
        <Row className="justify-content-center align-items-center h-100 flex-column">
          <Col className="text-center h-100">
            <div>
              <Logo />
            </div>

            <Col>
              <Row
                column
                className={`mt-4 justify-content-center flex-column align-items-center`}
              >
                <Button
                  size="lg"
                  color="primary"
                  className={styles.ButtonContainer}
                  onClick={() => setModalOpen(true)}
                >
                  Iniciar
                </Button>

                <Button
                  size="lg"
                  color="primary mt-3"
                  className={styles.ButtonContainer}
                >
                  Pontuação
                </Button>
              </Row>
            </Col>
          </Col>
          <Col className="mt-5 text-center text-dark">
            TIME 11 - BRASA Hacks
          </Col>
        </Row>
      </Container>
    </div>
  );
};

export default Home;
